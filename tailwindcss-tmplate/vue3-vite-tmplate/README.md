# Vue 3 + TypeScript + Vite

This template should help get you started developing with Vue 3 and TypeScript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

Learn more about the recommended Project Setup and IDE Support in the [Vue Docs TypeScript Guide](https://vuejs.org/guide/typescript/overview.html#project-setup).


## 模板

`加载状态`

```ts
import {ref} from "vue";

export default {
    components: {},
    setup() {
        const isLoading = ref(false)
        const handlerClicked = () => {
            isLoading.value = !isLoading.value

            let timer = setTimeout(() => {
                isLoading.value = !isLoading.value
                clearTimeout(timer)
            }, 1000)
        }

        return {
            isLoading,
            handlerClicked,
        }
    }
}
```