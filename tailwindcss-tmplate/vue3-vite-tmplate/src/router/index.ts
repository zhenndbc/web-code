// router.ts
import {createRouter, createWebHashHistory, RouteRecordRaw} from 'vue-router'


const routes: Array<RouteRecordRaw> = [
    {
        path: "/",
        redirect: '/home',
        children: [
            {
                path: "home",
                name: "首页",
                meta: {
                    title: "首页"
                },
                component: () => import('@/views/home/Home.vue')
            }
        ]
    },
    {
        path: "/components",
        children: [
            {
                path: "form",
                name: "FormView",
                meta: {
                    title: "表单页面"
                },
                component: () => import('@/components/DemoForm.vue'),
            },
            {
                path: "input",
                name: "InputView",
                meta: {
                    title: "输入页面"
                },
                component: () => import('@/components/DemoInput.vue'),
            },
            {
                path: "layout",
                name: "LayoutView",
                meta: {
                    title: "布局页面"
                },
                component: () => import('@/components/DemoLayout.vue'),
            },
            {
                path: "tabs",
                name: "TabsView",
                meta: {
                    title: "切换布局"
                },
                component: () => import('@/components/DemoTabs.vue'),
            },
            {
                path: "alicloud",
                name: "AlicloudView",
                meta: {
                    title: "阿里云盘"
                },
                component: () => import('@/components/AliCloud.vue'),
            },
        ]
    },
    {
        name: '404',
        path: '/:catchAll(.*)',
        component: () => import(`@/views/error/404.vue`)
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
})

router.beforeEach((to, _, next) => {
    const {meta: {title}} = to
    document.title = title as string || 'Title'

    next()
})

router.onError(() => {
    console.log("路由错误信息")
})

router.beforeResolve(() => {
    console.log("[INFO]", "前置解析")
})

export default router;