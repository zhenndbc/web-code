import {createApp} from 'vue';
import Qui from '@qvant/qui-max';
import '@qvant/qui-max/styles';

import router from '@/router'
import App from "@/App.vue"

const app = createApp(App);
// Setup all components
app.use(Qui)
app.use(router)
app.mount('#app')
