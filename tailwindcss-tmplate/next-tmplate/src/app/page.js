export default function Page () {
    return (
        <div className="w-1/3 p-2 mx-auto mt-10">
            <input className="w-full p-2 bg-gray-300 border outline-none hover:border-[#7fe7c4] focus:border-[#7fe7c4]" />
        </div>
    )
}